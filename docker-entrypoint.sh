#!/bin/bash

# Install more modules at runtime?
/bin/bash addModules.sh "${ADD_MODULES}"

# Execute Command
exec "$@"
