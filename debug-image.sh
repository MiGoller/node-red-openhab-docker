#!/bin/bash

if [ ! -d ./data ]
then
    mkdir -p data
    chmod -R 777 data
fi

APP_FLAVOR="rpi"

docker build \
    --build-arg ARG_NODE_VERSION=6 \
    --build-arg ARG_FLAVOR="${APP_FLAVOR}-" \
    --no-cache \
    -t node-red-openhab-docker:${APP_FLAVOR} .

docker rm -f node-red-openhab-docker

docker run -it --name node-red-openhab-docker \
    -p 1880:1880 \
    -v /etc/localtime:/etc/localtime:ro \
    -v /etc/timezone:/etc/timezone:ro \
    -v /srv/docker/node-red-openhab/dev/data:/data \
    node-red-openhab-docker:${APP_FLAVOR}
