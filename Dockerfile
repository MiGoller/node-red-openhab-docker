#
# "openHABian" Node-RED Docker image for openHAB
#

# Build arguments
ARG ARG_NODE_VERSION=6
ARG ARG_FLAVOR=""

FROM nodered/node-red-docker:${ARG_FLAVOR}v${ARG_NODE_VERSION}

# Build arguments ...
ARG ARG_APP_VERSION 

# The channel or branch triggering the build.
ARG ARG_APP_CHANNEL

# The commit sha triggering the build.
ARG ARG_APP_COMMIT

# Build data
ARG ARG_BUILD_DATE

# Basic build-time metadata as defined at http://label-schema.org
LABEL org.label-schema.build-date=${ARG_BUILD_DATE} \
    org.label-schema.docker.dockerfile="/Dockerfile" \
    org.label-schema.license="Apache License 2.0" \
    org.label-schema.name="MiGoller" \
    org.label-schema.vendor="MiGoller" \
    org.label-schema.version=${ARG_APP_VERSION} \
    org.label-schema.description="Node-RED for openHAB on Docker" \
    org.label-schema.url="https://gitlab.com/users/MiGoller/projects" \
    org.label-schema.vcs-ref=${ARG_APP_COMMIT} \
    org.label-schema.vcs-type="Git" \
    org.label-schema.vcs-url="https://gitlab.com/MiGoller/node-red-openhab-docker.git" \
    maintainer="MiGoller" \
    Author="MiGoller"

# Persist app-reladted build arguments
ENV APP_VERSION=$ARG_APP_VERSION \
    APP_CHANNEL=$ARG_APP_CHANNEL \
    APP_COMMIT=$ARG_APP_COMMIT

# Install "openHABian" nodes for openHAB 
RUN npm install node-red-contrib-bigtimer && \
      npm install node-red-contrib-openhab2

# Define volume for Node-RED userdata: Settings, flows, ...
VOLUME ["/data"]
