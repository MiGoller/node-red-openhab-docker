#!/bin/bash

#   latest, Node 6: ./dummy.sh "" "git" "" "true" "latest" "v6" 6
#   latest, Node 8: ./dummy.sh "" "git" "" "false" "" "v8" 8
#   slim, Node 6:   ./dummy.sh "slim" "git" "" "false" "slim" "-v6" 6
#   slim, Node 8:   ./dummy.sh "slim" "git" "" "false" "" "-v8" 8

set -e

###############################################################################
#   Build, tag and push the image.
###############################################################################
echo "-------------------------------------------------------------------------------"
echo "Parsing commandline parameters:"

export APP_FLAVOR=$1
export APP_VERSION=$2
export APP_SPECIAL_VERSIONS=$3
export APP_ISDEFAULT=$4
export APP_SPECIAL_TAGS=$5
export APP_SUFFIX=$6
export APP_DISTRO=$7

if [ -z "$BUILD_DATE" ]; then
    export BUILD_DATE=$(date +"%Y.%m.%d %H:%M:%S")
    echo " - Build date (set): $BUILD_DATE"
fi

echo " - Flavor:           $APP_FLAVOR"
echo " - Version:          $APP_VERSION"
echo " - Special versions: $APP_SPECIAL_VERSIONS"
echo " - Is default:       $APP_ISDEFAULT"
echo " - Special tags:     $APP_SPECIAL_TAGS"
echo " - Tag suffix:       $APP_SUFFIX"
echo " - Subfolder:        $APP_DISTRO"
echo ""

echo "Generating tags run createTagList.sh:"
eval "$( ./createTagList.sh "$APP_FLAVOR" "$APP_VERSION" "$APP_SPECIAL_VERSIONS" "$APP_ISDEFAULT" "$APP_SPECIAL_TAGS" "$APP_SUFFIX" "$APP_DISTRO" )"

echo " - $TAG_LIST"
echo ""

echo "Build and tag the image:"

tags=( ${TAG_LIST//,/ } )
BUILT_TAG=""
APP_NODE_VERSION=$APP_DISTRO

for tag in "${tags[@]}"; do

    if [ "${BUILT_TAG}" = "" ]; then
        # Build the Docker image with tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-${BUILT_TAG}"
        BUILT_TAG=${tag}
        echo "- Build image $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-${BUILT_TAG} ..."

        if [ -n "${APP_FLAVOR}" ]; then
            IMAGE_TYPE="${APP_FLAVOR}-"
        else
            IMAGE_TYPE=""
        fi

        docker build \
            --build-arg ARG_APP_VERSION=$APP_VERSION \
            --build-arg ARG_APP_CHANNEL=$CI_COMMIT_REF_SLUG \
            --build-arg ARG_APP_COMMIT=$CI_COMMIT_SHA \
            --build-arg ARG_BUILD_DATE="$BUILD_DATE" \
            --build-arg ARG_FLAVOR="$IMAGE_TYPE" \
            --build-arg ARG_NODE_VERSION=$APP_NODE_VERSION \
            -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-${BUILT_TAG}" .
    else
        # Tag and push image for each tag in list
        echo "- Tagging image:"
        echo "  - $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-${tag} ..."
        docker tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-${BUILT_TAG}" "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-${tag}"
    fi
done

# Build the Docker image with tag "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-${BUILT_TAG}"
echo "Push image and tags:"
for tag in "${tags[@]}"; do
    echo "  - $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-${tag} ..."
    docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-${tag}"
done

echo "-------------------------------------------------------------------------------"
echo "Building and tagging sequence:"
for tag in "${tags[@]}"; do
    echo "  - $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-${tag} "
done

echo "-------------------------------------------------------------------------------"
echo "Build succeeded!"
